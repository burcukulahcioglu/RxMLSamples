﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Disposables;
using System.Reactive.Concurrency;

using Accord.Math;

namespace ModelTemplate
{
    class Program
    {       
        static void Main(string[] args)
        {
            HMMEx hmm = new HMMEx();
            hmm.classifySequences();
           
            Console.ReadKey();
        }

    }
}
