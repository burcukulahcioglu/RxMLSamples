﻿using System;


using Accord.Statistics.Models.Fields;
using Accord.Statistics.Models.Fields.Functions;
using Accord.Statistics.Models.Fields.Learning;


using System.Reactive.Linq;

namespace ModelTemplate
{
    /**
     * Modified from the example provided in Accord.NET framework
     */

    class HMMEx
    {
        public void classifySequences()
        {
            // Suppose we would like to learn how to classify the
            // following set of sequences among three class labels:

            int[][] inputSequences =
            {
                // First class of sequences: starts and
                // ends with zeros, ones in the middle:
                new[] { 0, 1, 1, 1, 0 },
                new[] { 0, 0, 1, 1, 0, 0 },
                new[] { 0, 1, 1, 1, 1, 0 },

                // Second class of sequences: starts with
                // twos and switches to ones until the end.
                new[] { 2, 2, 2, 2, 1, 1, 1, 1, 1 },
                new[] { 2, 2, 1, 2, 1, 1, 1, 1, 1 },
                new[] { 2, 2, 2, 2, 2, 1, 1, 1, 1 },

                // Third class of sequences: can start
                // with any symbols, but ends with three.
                new[] { 0, 0, 1, 1, 3, 3, 3, 3 },
                new[] { 0, 0, 0, 3, 3, 3, 3 },
                new[] { 1, 0, 1, 2, 2, 2, 3, 3 },
                new[] { 1, 1, 2, 3, 3, 3, 3 },
                new[] { 0, 0, 1, 1, 3, 3, 3, 3 },
                new[] { 2, 2, 0, 3, 3, 3, 3 },
                new[] { 1, 0, 1, 2, 3, 3, 3, 3 },
                new[] { 1, 1, 2, 3, 3, 3, 3 },
            };

            // Now consider their respective class labels
            int[] outputLabels =
            {
                /* Sequences  1-3 are from class 0: */ 0, 0, 0,
                /* Sequences  4-6 are from class 1: */ 1, 1, 1,
                /* Sequences 7-14 are from class 2: */ 2, 2, 2, 2, 2, 2, 2, 2
            };


            // Create the Hidden Conditional Random Field using a set of discrete features
            var function = new MarkovDiscreteFunction(states: 3, symbols: 4, outputClasses: 3);
            var classifier = new HiddenConditionalRandomField<int>(function);

            // Create a learning algorithm
            var teacher = new HiddenResilientGradientLearning<int>(classifier)
            {
                Iterations = 50
            };


            /**
             * Converting training and test data into streams
             */

            // training input/output streams
            IObservable<int[]> inputStream = inputSequences.ToObservable();
            IObservable<int> outputStream = outputLabels.ToObservable();

            // train - the following line only trains with the last data !!
            //inputStream.Zip(outputStream, (lhs, rhs) => new { Left = lhs, Right = rhs }).Subscribe(x => teacher.Run(x.Left, x.Right));

            // Run the algorithm and learn the models
            teacher.Run(inputSequences, outputLabels);

            // operate on streaming data
            int[] y1 = new[] { 0, 1, 1, 1, 0 };    // output is y1 = 0
            int[] y2 = new[] { 0, 0, 1, 1, 0, 0 }; // output is y1 = 0

            int[] y3 = new[] { 2, 2, 2, 2, 1, 1 }; // output is y2 = 1
            int[] y4 = new[] { 2, 2, 1, 1 };       // output is y2 = 1

            int[] y5 = new[] { 0, 0, 1, 3, 3, 3 }; // output is y3 = 2
            int[] y6 = new[] { 2, 0, 2, 2, 3, 3 }; // output is y3 = 2

            int[][] testData = { y1, y2, y3, y4, y5, y6 };

            testData.ToObservable().Subscribe(x => Console.WriteLine("Sequence {{{0}}} \t is classified as: \t {1}", string.Join(", ", x), classifier.Compute(x)));

            Console.ReadKey();
        }
    }
}
