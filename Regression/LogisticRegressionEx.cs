﻿using System;

using Accord.Statistics.Models.Regression;
using Accord.Statistics.Models.Regression.Fitting;


namespace StreamOps
{
    /**
    * Modified from the example provided in Accord.NET framework
    */

    class LogisticRegressionEx
    {
        LogisticRegression regression;

        public void trainRegression()
        {
            // age and smoke
            double[][] input =
            {
                new double[] { 55, 0 }, // 0 - no cancer
                new double[] { 28, 0 }, // 0
                new double[] { 65, 1 }, // 0
                new double[] { 46, 0 }, // 1 - have cancer
                new double[] { 86, 1 }, // 1
                new double[] { 56, 1 }, // 1
                new double[] { 85, 0 }, // 0
                new double[] { 33, 0 }, // 0
                new double[] { 21, 1 }, // 0
                new double[] { 42, 1 }, // 1
            };

            double[] output =
            {
                0, 0, 0, 1, 1, 1, 0, 0, 0, 1
            };

            // To verify this hypothesis, we are going to create a logistic
            // regression model for those two inputs (age and smoking).
            regression = new LogisticRegression(inputs: 2);

            // Next, we are going to estimate this model. For this, we
            // will use the Iteratively Reweighted Least Squares method.
            var teacher = new IterativeReweightedLeastSquares(regression);

            // Now, we will iteratively estimate our model. The Run method returns
            // the maximum relative change in the model parameters and we will use
            // it as the convergence criteria.

            double delta = 0;
            do
            {
                // Perform an iteration
                delta = teacher.Run(input, output);

            } while (delta > 0.001);

            /** An idea is to convert training data into stream and train regression with the stream
             *  But IterativeReweightedLeastSquares does not support one by one training 
             */
        }

        public void runForSampleInputs()
        {
            double[][] inputData =             {
                new double[] {  52,  1 }, // input sample 1
                new double[] {  52,  0 }, // input sample 2
                new double[] {  18,  0 }, // input sample 3
                new double[] {  18,  1 }
            };

            // convert test data into stream
            var paced = GenericML.toPacedObservable(inputData, 1000);

            GenericML.GenericMLToolMethod<double[], double> logisticRegressionMethod = regression.Compute;

            IObservable<double> results = GenericML.applyToolBox(paced, logisticRegressionMethod);

            results.Subscribe(x => Console.WriteLine("The output (has cancer or not) for the input sample: {0}", x));
        }
    }
}
