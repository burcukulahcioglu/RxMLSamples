﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace StreamOps
{
    class Program
    {
        static void Main(string[] args)
        {
            LogisticRegressionEx logRegression = new LogisticRegressionEx();
            logRegression.trainRegression();
            logRegression.runForSampleInputs();

            Console.ReadKey();
        }
    }
}
