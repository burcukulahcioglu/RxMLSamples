﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Disposables;
using System.Reactive.Concurrency;

namespace StreamOps
{
    class GenericML
    {
        public delegate TOutput GenericMLToolMethod<TInput, TOutput>(TInput input);
        public delegate TOutput GenericAggregareMethod<TOutput, TInput>(TOutput acc, TInput currentVal);

        // transform a string to another one (e.g. regression to estimate y from x, noise elimination)
        public static IObservable<TOut> applyToolBox<TIn, TOut>(IObservable<TIn> stream, GenericMLToolMethod<TIn, TOut> method)
        {
            return stream.Select(x => method(x));
        }

        // output a result based on recent history (e.g. produce max of last 5 items (e.g. mostly used hashtag in last n messages)) 
        public static IObservable<IObservable<TOut>> applyToolBoxToRecentInNum<TIn, TOut>(IObservable<TIn> stream, GenericAggregareMethod<TOut, TIn> method, int windowCount, int skipCount, TOut seed)
        {
            return stream.Scan(seed, (acc, currentValue) => method(acc, currentValue)).Window(count: windowCount, skip: skipCount);
        }

        // convert enumeravle into a paced stream
        public static IObservable<T> toPacedObservable<T>(IEnumerable<T> data, int paceMSec)
        {
            return data.Select(i => Observable.Empty<T>()
                                        .Delay(TimeSpan.FromMilliseconds(paceMSec))
                                        .StartWith(i)).Concat();
        }
    }
}
